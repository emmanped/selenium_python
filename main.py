from selenium import webdriver
import config


class BaseClass:
    def __init__(self, chromedriver, url):
        self.chromedriver = chromedriver
        self.url = url

    def go_to_url(self):

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("start-maximized")
        driver = webdriver.Chrome(self.chromedriver, options=chrome_options)

        try:
            driver.get(self.url)
            driver.quit()
        except:
            driver.quit()


base_class = BaseClass(chromedriver=config.chrome_driver, url=config.base_url)

base_class.go_to_url()
